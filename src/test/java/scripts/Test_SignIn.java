package scripts;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Test_SignIn extends Base {

    @Test(priority = 1, description = "Case 1: Validate Cars.com Sign in page form")
    public void SignIn() {
        driver.get("https://www.cars.com/");
        carsHomePage.loginButton.click();
        Assert.assertTrue(carsSignInPage.heading1.isDisplayed());
        Assert.assertEquals(carsSignInPage.heading1.getText(), "Sign in");
        Assert.assertTrue(carsSignInPage.paragraph.isDisplayed());
        Assert.assertEquals(carsSignInPage.paragraph.getText(), "New to Cars.com? Sign up. Are you a dealer?");
        Assert.assertTrue(carsSignInPage.emailInput.isDisplayed() && carsSignInPage.emailInput.isEnabled());
        Assert.assertEquals(carsSignInPage.emailInput.getText(), "Email");
        Assert.assertTrue(carsSignInPage.passwordInput.isDisplayed() && carsSignInPage.passwordInput.isEnabled());
        Assert.assertEquals(carsSignInPage.passwordInput.getText(), "Password");
        Assert.assertTrue(carsSignInPage.warning.isDisplayed());
        Assert.assertEquals(carsSignInPage.warning.getText(), "Minimum of eight characters");
        Assert.assertTrue(carsSignInPage.forgotPassword.isDisplayed() && carsSignInPage.forgotPassword.isEnabled());
        Assert.assertEquals(carsSignInPage.forgotPassword.getText(), "Forgot password?");
        Assert.assertEquals(carsSignInPage.privacyText.getText(), "By signing in to your profile, you agree to our  Privacy Statement  and  Terms of Service.");
        Assert.assertTrue(carsSignInPage.signInButton.isDisplayed() && carsSignInPage.signInButton.isEnabled());
        Assert.assertEquals(carsSignInPage.signInButton.getText(), "Sign in");

    }

    @Test(priority = 2, description = "Case 2: Validate Cars.com Sign in page social media section")
    public void SignInWithSocial() {
        driver.get("https://www.cars.com/");
        carsHomePage.loginButton.click();
        Assert.assertTrue(carsSignInPage.connectSocial.isDisplayed());
        Assert.assertTrue(carsSignInPage.signInFacebook.isDisplayed() && carsSignInPage.signInFacebook.isEnabled());
        Assert.assertEquals(carsSignInPage.signInFacebook.getText(), "Sign in with Facebook");
        Assert.assertTrue(carsSignInPage.getSignInGoogle.isDisplayed() && carsSignInPage.getSignInGoogle.isEnabled());
        Assert.assertEquals(carsSignInPage.getSignInGoogle.getText(), "Sign in with Google");
        Assert.assertTrue(carsSignInPage.getSignInApple.isDisplayed() && carsSignInPage.getSignInApple.isEnabled());
        Assert.assertEquals(carsSignInPage.getSignInApple.getText(), "Sign in with Apple");

    }

    @Test(priority = 3, description = "Case 3: Validate user cannot sign in to Cars.com with invalid credentials")
    public void InputTest() {
        driver.get("https://www.cars.com/");
        carsHomePage.loginButton.click();
        carsSignInPage.emailInputTorKeys.sendKeys("johndoe@gmail.com");
        carsSignInPage.passwordInputTorKeys.sendKeys("abcd1234");
        carsSignInPage.signInButton.click();
        Assert.assertEquals(carsSignInPage.errorMessage.getText(),
                "We were unable to sign you in.\n" +
                        "Your email or password was not recognized. Try again soon.");
    }
}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarsSignInPage {
    public CarsSignInPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//h1[text()='Sign in']")
    public WebElement heading1;

    @FindBy(xpath = "//p[@class='sds-page-section__title--sub signup-block']")
    public WebElement paragraph;

    @FindBy(xpath = "//input[@id='email']/following-sibling::label")
    public WebElement emailInput;
    @FindBy(id = "email")
    public WebElement emailInputTorKeys;

    @FindBy(xpath = "//input[@id='password']/following-sibling::label")
    public WebElement passwordInput;
    @FindBy(id = "password")
    public WebElement passwordInputTorKeys;


    @FindBy(xpath = "//p[text()='Minimum of eight characters']")
    public WebElement warning;

    @FindBy(xpath = "//a[text()='Forgot password?']")
    public WebElement forgotPassword;

    @FindBy(xpath = "//div[@class='sds-disclaimer ']")
    public WebElement privacyText;

    @FindBy(xpath = "//button[text()='Sign in']")
    public WebElement signInButton;

    @FindBy(xpath = "//h3[text()='Connect with social']")
    public WebElement connectSocial;

    @FindBy(xpath = "//a[@data-linkname='signin-social-facebook']")
    public WebElement signInFacebook;

    @FindBy(xpath = "(//a[@data-linkname='signin-social-google'])[1]")
    public WebElement getSignInGoogle;

    @FindBy(xpath = "(//a[@data-linkname='signin-social-google'])[2]")
    public WebElement getSignInApple;

//    @FindBy(xpath = "//button[@class='sds-button']")
//    public WebElement signInButton2;

    @FindBy(xpath = "//div[@class='sds-notification sds-notification--error']")
    public WebElement errorMessage;

}
